package com.moviexx2.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.moviexx2.customer.entity.Movies;

@RestController
@RequestMapping("/kafka")
public class KafkaController {
    
    @Autowired
	private KafkaTemplate<Object, Object> template;
    
    @PostMapping(path = "/send/movies")
	public void send(@RequestBody Movies movies) {
		this.template.send("topic.movies", movies);
	}
}
