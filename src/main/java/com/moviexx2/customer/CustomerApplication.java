package com.moviexx2.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

import com.moviexx2.customer.entity.Movies;
import com.moviexx2.customer.service.MoviesService;

@SpringBootApplication
public class CustomerApplication {
	@Autowired
	MoviesService service;

	public static void main(String[] args) {
		SpringApplication.run(CustomerApplication.class, args);
	}

	@KafkaListener(id ="movieListener", topics = "topic.movies")
	void movieListener(Movies movies) {
		service.addMovies(movies);
		System.out.println("Received: " + movies);
	}

}
